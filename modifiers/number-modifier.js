const DataNumber  = require('../data/number');

function numberModifier(inputNumber) {
/* ***
 *
 * You can only use divisions
 * 
 * Return expected: 159
 * 
 * 
*** */ 
  return DataNumber / 16;
}

module.exports = {
  numberModifier,
};
