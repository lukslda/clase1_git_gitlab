const DataArray  = require('../data/array');

function arrayModifier (inputArray) {
/* ***
 *
 * Return expected: ['paml', 'rouf', 'moor', 'lalh']
 * 
 * last change
 *** */ 
    const Array = DataArray.map(oneData => {
        let initialLetter = []
        let lastLetter = []
        let restLetter = []
        let newArray = []
        initialLetter.push(oneData[0]);
        lastLetter.push(oneData[3]);
        restLetter.push(oneData[1] + oneData[2],);
        newArray.push(lastLetter+restLetter+initialLetter);
        return newArray[0];
    });
    return Array;
};

module.exports = {
    arrayModifier
};